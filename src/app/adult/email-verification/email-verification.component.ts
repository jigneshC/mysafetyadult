import { Component, OnInit } from '@angular/core';

import { CommonEventsService } from "../../common-events.service";
import { AuthApiService } from '../../auth-api.service';
import { ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.css']
})
export class EmailVerificationComponent implements OnInit {
  public authuser: any = {};
  public input_1: any = null;
  public input_2: any = null;
  public input_3: any = null;
  public input_4: any = null;
  public input_5: any = null;
  public input_6: any = null;
  public email: any = null;

  submitted = false;
  formload = false;

  constructor(public api: AuthApiService,private subscription: CommonEventsService,private route: ActivatedRoute,private router: Router) {

  }

  ngOnInit() {
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));

    if(authuser.email_verified_at != ""){
      this.router.navigate(['account']);
      return false;
    }
    this.email = authuser.email;
    this.authuser = authuser;
  }
  numberOnly(event,nextfocusid): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    let element: HTMLElement = document.getElementById(nextfocusid) as HTMLElement;
    element.focus();
    return true;

  }

  private prepareSave(): any {


    let input = { email: this.email, otp: this.input_1.toString()+this.input_2.toString()+this.input_3.toString()+this.input_4.toString()+this.input_5.toString()+this.input_6.toString() };

    return input;
  }
  onSubmit() {
    this.submitted = true;



   if (this.input_1 === null || this.input_2 === null || this.input_3 === null || this.input_4 === null || this.input_5 === null || this.input_6 === null) {
        return;
   }else{
      this.formload = true;
      let myData = this.prepareSave();
      this.api.submitAuth("verify-email",myData).subscribe(result => {
		this.formload = false;
        let sucsess_ob =  {action:'flash_success_sweet',redirect_to:'account',dataobj:{'message':result.message,'message_head':'Success !'}};
        this.authuser.email_verified_at = result.data.user.email_verified_at;

        localStorage.setItem('authuser',JSON.stringify(this.authuser));
        this.subscription.globleEvent({action:'load_notification',redirect_to:'',dataobj:{'message':'','message_head':''}});
        this.subscription.globleEvent(sucsess_ob);
      },
      (error) => {this.displayError(error);})
   }
 }

 resend() {
  this.submitted = true;



 if (0) {
      return;
 }else{
    this.formload = true;

    let myData = { email: this.email };
    this.api.submitAuth("verify-email-resend",myData).subscribe(result => {
		this.formload = false;
      let sucsess_ob =  {action:'flash_success_sweet',redirect_to:'',dataobj:{'message':result.message,'message_head':'Success !'}};
      this.subscription.globleEvent(sucsess_ob);
    },
    (error) => {this.displayError(error);})
 }
}

 displayError(error:any) {
  this.formload = false;
  let errMsg = (error.message) ? error.message :
  error.status ? `${error.status} - ${error.statusText}` : 'Server error';

  if(error.error && error.error.message){
    errMsg = error.error.message
  }
  let error_ob =  {action:'flash_error_sweet',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
  this.subscription.globleEvent(error_ob);
}





}
