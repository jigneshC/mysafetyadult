import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TextMaskModule } from 'angular2-text-mask';
import { Ng2TelInputModule} from 'ng2-tel-input';
import { MatDatepickerModule, MatInputModule,MatNativeDateModule} from '@angular/material';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxStripeModule } from 'ngx-stripe';



import { AppRoutingModule } from './app-routing.module';
import { NgOtpInputModule } from  'ng-otp-input';

import { LayoutComponent } from './layout/layout.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { ProfileComponent } from './profile/profile.component';
import { ConnectionComponent } from './connection/connection.component';
import { PhoneVerificationComponent } from './phone-verification/phone-verification.component';
import { ProfileVerificationComponent } from './profile-verification/profile-verification.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { LoaderComponent } from './loader/loader.component';

import { config } from "../app-data";




@NgModule({
  declarations: [LayoutComponent, MyAccountComponent, ProfileComponent, ConnectionComponent, PhoneVerificationComponent, ProfileVerificationComponent,
     EmailVerificationComponent,LoaderComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    AppRoutingModule,
    NgOtpInputModule,
	TextMaskModule,
	Ng2TelInputModule,
	MatDatepickerModule, MatInputModule,MatNativeDateModule,
	NgxDropzoneModule,
	NgxStripeModule.forRoot(config.PUBLISHABLE_KEY),
	
  ]
})
export class AdultModule { }
