import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { ActivatedRoute,Router} from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database'; 

import * as moment from 'moment';

import { AuthApiService } from '../../auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { ccodes } from "./../../c-codes";
import { EditChildValidation } from '../../service/validation/edit-child-validation';




@Component({
  selector: 'app-phone-verification',
  templateUrl: './phone-verification.component.html',
  styleUrls: ['./phone-verification.component.css']
})
export class PhoneVerificationComponent implements OnInit {
 public authuser: any = {};	
  
  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  signinForm : FormGroup;
  
  public input_1: any = null;
  public input_2: any = null;
  public input_3: any = null;
  public input_4: any = null;
  
  
  public verificationData: any = {};
  public call_status: number = 1;
  phoOb :any = null;
  country_code : any = "+91";
  country_code_t : any = "us";
  phone : any = "";
  phone_verified : any = "0";

  signupForm : FormGroup;
  
  submitted = false;
  formload = false;
  
  success:boolean = false;
  call:boolean = false;
  showTimer:boolean = true;

  timeLeft: number = 10;
  interval;
  
  constructor(private fib: AngularFireDatabase,formBuilder: FormBuilder,public api: AuthApiService,private subscription: CommonEventsService,private route: ActivatedRoute,private router: Router) {
	let authuser: any = JSON.parse(localStorage.getItem("authuser"));

	if(!authuser){
	  this.router.navigate(['sign-in']);
    }
	
	if(authuser.phone_verified == "1"){
	  this.success = true;
      this.router.navigate(['account']);
    }
    if(!this.success){
		this.phone = authuser.phone;
		this.phone_verified = authuser.phone_verified;
		this.country_code = authuser.country_code;
		
		for (let i = 0; i < ccodes.length; i++) {
			if (ccodes[i].dial_code == this.country_code) {
				this.country_code_t = ccodes[i].code;
				break;
			}
		}
		
		this.authuser = authuser;
		
		let items = fib.object('phone-verification/'+this.authuser.id).valueChanges().subscribe(items => {
		  this.checkisVerified(items);
		});
	}
    this.signupForm = formBuilder.group({
      'country_code':this.country_code,
      'phone' : [this.phone, Validators.compose([Validators.required,Validators.maxLength(15)])],
    },{'validator': EditChildValidation.Formvalidation});

  }

  private prepareSave(): any {

    let input = new FormData();
    
    input.append('country_code', this.country_code);
    input.append('otp',this.verificationData.otp);
    let aphone = this.signupForm.get('phone').value;
	this.phone = aphone.replace(/[^0-9]/g, '');
    input.append('phone', this.phone);
   
    return {'country_code':this.country_code,'otp':this.verificationData.otp,'phone':aphone.replace(/[^0-9]/g, '')};
  }

  ngOnInit() {
	
	this.getPhonedata("");
  }
  getPhonedata(myData: any) {
    this.formload = true;
    this.api.submitAuth("get-phoneverification-code", myData).subscribe(result => {
      this.formload = false;
      this.verificationData = result.data;
	  this.country_code = this.verificationData.country_code;
	  let digits = (this.verificationData.otp).split('');
	  
	  for (let i = 0; i < digits.length; i++) {
        if (i == 0) { this.input_1 = digits[i]; }
        else if (i == 1) { this.input_2 = digits[i]; }
        else if (i == 2) { this.input_3 = digits[i]; }
        else if (i == 3) { this.input_4 = digits[i]; }
      }
	
    },(error) => { this.formload = false; })
  }
  checkisVerified(verifingdata:any){
    
    let authuser: any = JSON.parse(localStorage.getItem("authuser"));
    if(verifingdata && verifingdata.user_id && this.phone!= ""){
	   let my_phone_no = this.country_code+this.phone;
	   console.log(verifingdata);
      if(verifingdata.user_id == authuser.id && verifingdata.phone_number == my_phone_no && verifingdata.verified == 1){
        this.call_status = 3;
        authuser.phone_verified = 1;
        authuser.country_code = this.country_code;
        authuser.phone =this.phone;
        localStorage.setItem('authuser',JSON.stringify(authuser));
		this.startTimer(true);
      }else{
        this.call_status = 2;
      }
    }
  }
  
  redirectifverified(){
    let authuser = JSON.parse(localStorage.getItem("authuser"));
	
    
	clearInterval(this.interval);
    if(authuser && authuser.phone_verified == 1){
      let sucsess_ob =  {action:'flash_success_sweet',redirect_to:'account',dataobj:{'message':"Thank you! Your phone number has been verified.",'message_head':'Success !'}};
      this.subscription.globleEvent(sucsess_ob);
	  this.closemodal();
    }

    
    this.call_status = 0;
  }
  
  gotoRoute(route:string) {
	clearInterval(this.interval);
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  
   getNumber(obj) {
    // console.log(obj);
   }
   dochangeCountry(dcode:any){
   for (let i = 0; i < ccodes.length; i++) {
        if (ccodes[i].dial_code == dcode) {
       this.phoOb.intlTelInput('setCountry',ccodes[i].code);
       break;
        }
     }

   }
   onCountryChange(obj) {
    this.country_code = "+"+obj.dialCode;
   }

   hasError(obj) {
     //console.log(obj);
   }
   telInputObject(obj) {
     this.phoOb = obj;
     this.dochangeCountry(this.country_code);
   }

  
  
  onSubmit() {
    
	
	
    this.submitted = true;

    if (this.signupForm.invalid) {
      return;
    }else{

	  this.formload = true;	
      let formvalue = this.prepareSave();

      this.api.submitAuth("phoneverification-call",formvalue).subscribe(result => {
		this.formload = false;
		this.call_status = 1;
		let element: HTMLElement = document.getElementById("submit_btn") as HTMLElement;
		element.click();
	
      },
      (error) => { this.formload = false; this.displayError(error); })
   }

 }
 
 closemodal() {
    let element: HTMLElement = document.getElementById("close-btn") as HTMLElement;
	element.click();
  }
  
    
 ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
  startTimer(data){
    if(data = true){
      this.interval = setInterval(() => {
        if(this.timeLeft > 0) {
          this.timeLeft--;
          if(this.timeLeft < 10){}
        } else {
          this.timeLeft = 10;
          this.redirectifverified();
        }
      },1000)
    }
  }

 displayError(error:any) {
  
  let errMsg = (error.message) ? error.message :
  error.status ? `${error.status} - ${error.statusText}` : 'Server error';

  if(error.error && error.error.message){
    errMsg = error.error.message
  }
  let error_ob =  {action:'flash_error_sweet',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
  this.subscription.globleEvent(error_ob);
}

  
}
