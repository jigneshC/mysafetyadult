import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { LayoutComponent } from './layout/layout.component';
import { ProfileComponent } from './profile/profile.component';
import { ConnectionComponent } from './connection/connection.component';
import { PhoneVerificationComponent } from './phone-verification/phone-verification.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { ProfileVerificationComponent } from './profile-verification/profile-verification.component';


import { PhoneVerificationGuard } from '../guard/phone-verification.guard';

const routes: Routes = [

  {path: '', component:LayoutComponent, children: [
    {path: '', canActivate:[PhoneVerificationGuard], children: [
      {
        path: '',
        component: ProfileComponent
      },
      {
        path: 'connections',
        component: ConnectionComponent
      }
    ]},
    {
      path: 'phone-verification',
      component: PhoneVerificationComponent
    },
    {
      path: 'email-verification',
      component: EmailVerificationComponent
    },
    {
      path: 'profile-verification',
      component: ProfileVerificationComponent
    }
  ]}

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

