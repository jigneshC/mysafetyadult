import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router} from '@angular/router';
import { ISubscription } from "rxjs/Subscription";
import Swal from 'sweetalert2';
import { Meta , Title } from '@angular/platform-browser';

import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

import { CommonEventsService } from "../../common-events.service";
import { AuthApiService } from '../../auth-api.service';
import { config } from "../../app-data";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  public config: any = config;
  private subscription: ISubscription;
  public isHomePage: boolean = true;
  public module: string = 'default';

  public authusername: string = '';
  public notification: any = [];
  public child_notification: any = [];
  public items: any = {};
  public seodata: any = [];

  constructor(public title:Title,public meta: Meta,private _scrollToService: ScrollToService, public api: AuthApiService, private route: ActivatedRoute, private router: Router, private jevent: CommonEventsService) {

  }

  ngOnInit() {
    this.subscription = this.jevent.currentData.subscribe(
      (data: any) => {
      /*  if (data.action == 'flash_success') {
          this.toastr.success(data.dataobj.message, data.dataobj.message_head);
        }

        if (data.action == 'flash_error') {
          this.toastr.error(data.dataobj.message, data.dataobj.message_head);
        }
        if (data.action == 'flash_warning') {
          this.toastr.warning(data.dataobj.message, data.dataobj.message_head);
        }
        if (data.action == 'flash_info') {
          this.toastr.info(data.dataobj.message, data.dataobj.message_head);
        }
        if (data.action == 'scroll_top') {
          this.triggerScrollTo();
        }
        if (data.action == 'set_blog_meta') {
          this.setMetatagsBlog(data.dataobj);
        }
*/
      if (data.action == 'flash_success_sweet') {
        this.sweetalert(data.dataobj.message_head,'success',data.dataobj.message);
      }
      if (data.action == 'flash_error_sweet') {
        this.sweetalert(data.dataobj.message_head,'warning',data.dataobj.message);
      }

        if (data.redirect_to && data.redirect_to != '' && data.redirect_to != 'undefined') {
          this.gotoRoute(data.redirect_to, false);
        }
      });

  }
  sweetalert(title:string,type,message,confbtn = 'Continue'){
    Swal.fire({
      title: title,
      text: message,
      icon: type,
      confirmButtonText: confbtn,
      confirmButtonColor: "#9959f2",
    });
  }
  public triggerScrollTo() {

    const config: ScrollToConfigOptions = { target: 'wrapper' };
    this._scrollToService.scrollTo(config);
  }
  ngOnDestroy() {
  if(this.subscription){
    this.subscription.unsubscribe();
	}
  }

  doLogout() {

	
	
    this.api.doLogout("logout", []).subscribe(result => {
		localStorage.removeItem('authuser');
      this.gotoRoute('sign-in', false);
	},
      (error) => {
        this.gotoRoute('sign-in', false);
      });

	localStorage.removeItem('authuser');
  }
  gotoRoute(route: string, isselfpage: boolean) {


    this.router.navigate(['/' + route]);
  }
}
