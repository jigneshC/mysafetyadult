import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { HttpClientModule } from '@angular/common/http';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AuthApiService } from './auth-api.service';
import { CommonEventsService } from "./common-events.service";



import { environment } from '../environments/environment';
import { GtagModule } from 'angular-gtag';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [

    HttpClientModule,
    BrowserModule,BrowserAnimationsModule,
    AppRoutingModule,
    ScrollToModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    GtagModule.forRoot(environment.gaConfig),
    AngularFireDatabaseModule,
  ],
  providers: [CommonEventsService,AuthApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
