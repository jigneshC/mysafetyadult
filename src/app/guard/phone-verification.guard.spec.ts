import { TestBed, async, inject } from '@angular/core/testing';

import { PhoneVerificationGuard } from './phone-verification.guard';

describe('PhoneVerificationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PhoneVerificationGuard]
    });
  });

  it('should ...', inject([PhoneVerificationGuard], (guard: PhoneVerificationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
