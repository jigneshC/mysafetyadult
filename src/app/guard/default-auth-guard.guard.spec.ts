import { TestBed, async, inject } from '@angular/core/testing';

import { DefaultAuthGuardGuard } from './default-auth-guard.guard';

describe('DefaultAuthGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DefaultAuthGuardGuard]
    });
  });

  it('should ...', inject([DefaultAuthGuardGuard], (guard: DefaultAuthGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
