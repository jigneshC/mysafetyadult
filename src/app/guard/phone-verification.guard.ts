import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PhoneVerificationGuard implements CanActivate {

  constructor(private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let authuser = JSON.parse(localStorage.getItem("authuser"));

      if(authuser && authuser.email_verified_at == ""){
        this.router.navigate(['account/email-verification']);
        return false;
      }else if(authuser && authuser.phone_verified != 1){
        this.router.navigate(['account/phone-verification']);
        return false;
      }else if(authuser && authuser.badge_status != "approved"){
        this.router.navigate(['account/profile-verification']);
        return false;
      }

      return true;
  }

}
