import { TestBed, async, inject } from '@angular/core/testing';

import { AdultAuthGuardGuard } from './adult-auth-guard.guard';

describe('AdultAuthGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdultAuthGuardGuard]
    });
  });

  it('should ...', inject([AdultAuthGuardGuard], (guard: AdultAuthGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
