import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnonymousComplaintComponent } from './anonymous-complaint.component';

describe('AnonymousComplaintComponent', () => {
  let component: AnonymousComplaintComponent;
  let fixture: ComponentFixture<AnonymousComplaintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnonymousComplaintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnonymousComplaintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
