import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { Ng2TelInputModule} from 'ng2-tel-input';
import { MatDatepickerModule, MatInputModule,MatNativeDateModule} from '@angular/material';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxStripeModule } from 'ngx-stripe';

import { NgwWowModule } from 'ngx-wow';

import { AppRoutingModule } from './app-routing.module';

import { LayoutComponent } from './layout/layout.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { config } from "../app-data";
import { LoaderComponent } from './loader/loader.component';
import { LandingComponent } from './landing/landing.component';
import { FeaturesComponent } from './features/features.component';
import { PricingComponent } from './pricing/pricing.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { SupportComponent } from './support/support.component';
import { AnonymousComplaintComponent } from './anonymous-complaint/anonymous-complaint.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';








@NgModule({
  declarations: [LayoutComponent, AboutComponent, ContactComponent, SignInComponent, SignUpComponent, ForgotPasswordComponent, LoaderComponent, LandingComponent, FeaturesComponent, PricingComponent, BlogsComponent, SupportComponent, AnonymousComplaintComponent, PrivacyComponent, TermsConditionsComponent, BlogDetailComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    AppRoutingModule,
    TextMaskModule,
	NgwWowModule,
    NgxDropzoneModule,
    MatDatepickerModule, MatInputModule,MatNativeDateModule,
    Ng2TelInputModule,
    NgxStripeModule.forRoot(config.PUBLISHABLE_KEY),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ]
})
export class DefaultModule { }
