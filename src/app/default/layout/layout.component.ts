import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute,Router} from '@angular/router';
import { ISubscription } from "rxjs/Subscription";
import Swal from 'sweetalert2';
import { Meta , Title } from '@angular/platform-browser';


import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

import { CommonEventsService } from "../../common-events.service";
import { AuthApiService } from '../../auth-api.service';
import { config } from "../../app-data";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  public config: any = config;
  private subscription: ISubscription;
  public isHomePage: boolean = true;
  public module: string = 'default';

  public authusername: string = '';
  public notification: any = [];
  public child_notification: any = [];
  public items: any = {};
  public seodata: any = [];
  public curl: string = '';
  constructor(public title:Title,public meta: Meta,private _scrollToService: ScrollToService, public api: AuthApiService, private route: ActivatedRoute, private router: Router, private jevent: CommonEventsService) {
		this.curl = this.router.url;
		
  }

  
  ngOnInit() {
	
    this.getMetatags();
    this.subscription = this.jevent.currentData.subscribe(
      (data: any) => {
  
      if (data.action == 'flash_success_sweet') {
        this.sweetalert(data.dataobj.message_head,'success',data.dataobj.message);
      }
      if (data.action == 'flash_error_sweet') {
        this.sweetalert(data.dataobj.message_head,'warning',data.dataobj.message);
      }

        if (data.redirect_to && data.redirect_to != '' && data.redirect_to != 'undefined') {
          this.gotoRoute(data.redirect_to, false);
        }
      });

  }
  sweetalert(title:string,type,message,confbtn = 'Continue'){
    Swal.fire({
      title: title,
      text: message,
      icon: type,
      confirmButtonText: confbtn,
      confirmButtonColor: "#9959f2",
    });
  }
  public triggerScrollTo() {

    const config: ScrollToConfigOptions = { target: 'wrapper' };
    this._scrollToService.scrollTo(config);
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  getMetatags(){

    this.api.getData("get-seopage",[]).subscribe(result => {
      this.seodata = result.result;
      var ruts = this.router.url;
      this.setMetatags(ruts);
    },
    (error) => {  })
  }
  setMetatags(route:string){

    var ruts = route.split("/");
    if (Number(ruts[ruts.length-1])){
      ruts.pop();
    }
    if(ruts[0] == ""){
      ruts.splice(0, 1);
    }
    if(ruts[0] == "blog"){
     return false;
    }
    var slug = ruts.join("-");

    if(route == "" || route =="/"){
      slug = "home";
    }


    let mdata = this.seodata.find(x => x.page_name == slug);
    if(mdata && config.live == 1){+
      this.title.setTitle(mdata.title);
      this.meta.updateTag({ name: 'description', content: mdata.description });
      this.meta.updateTag({ name: 'title', content: mdata.title });
      this.meta.updateTag({ name: 'keywords', content: mdata.keywords });
    }

  }
  setMetatagsBlog(blog:any){
    let mdata = this.seodata.find(x => x.page_name == 'default');
    this.title.setTitle(blog.title);
    this.meta.updateTag({ name: 'title', content: blog.title });
    if(blog.sort_desc && blog.sort_desc != ""){
      this.meta.updateTag({ name: 'description', content: blog.sort_desc });
    }else{
      this.meta.updateTag({ name: 'description', content: mdata.description });
    }

    if(blog.meta_keywords && blog.meta_keywords != ""){
      this.meta.updateTag({ name: 'keywords', content: blog.meta_keywords });
    }else{
      this.meta.updateTag({ name: 'keywords', content: mdata.keywords });
    }


  }

  gotoRoute(route: string, isselfpage: boolean) {

	this.curl = route;
    this.setMetatags(route);
    if (this.router.url == "/" && isselfpage) {
      return true;
    } else {
      this.router.navigate(['/' + route]);
    }
    this.triggerScrollTo();

  }

}
