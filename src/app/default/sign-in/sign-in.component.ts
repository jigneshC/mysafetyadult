import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthApiService } from '../../auth-api.service';
import { CommonEventsService } from "../../common-events.service";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  signinForm : FormGroup;
  submitted = false;
  formload = false;
  viewPassword = false;

  constructor(formBuilder: FormBuilder,public api: AuthApiService,private subscription: CommonEventsService) {

    this.signinForm = formBuilder.group({
      'password' : [null, Validators.required],
      'email' :[null, Validators.required],
    });

  }

  ngOnInit() {
  }

  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  onSubmit() {
    this.submitted = true;
    let myData =this.signinForm.value;



   if (this.signinForm.invalid) {
        return;
   }else{
      this.formload = true;
      myData.device_type='web';
      this.api.submit("login",myData).subscribe(result => {

        this.enableForm();
        let sucsess_ob =  {action:'flash_success_sweet',redirect_to:'account',dataobj:{'message':result.message,'message_head':'Success !'}};
        let authdata = result.data.user;

        localStorage.setItem('authuser',JSON.stringify(authdata));
        this.subscription.globleEvent({action:'load_notification',redirect_to:'',dataobj:{'message':'','message_head':''}});
        this.subscription.globleEvent(sucsess_ob);
      },
      (error) => {this.displayError(error);})
   }
 }

 togglePasswordView(){
   if(this.viewPassword){ this.viewPassword =false; }else{ this.viewPassword =true; }
 }
 displayError(error:any) {
    this.enableForm();
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    if(error.error && error.error.message){
      errMsg = error.error.message
    }
    let error_ob =  {action:'flash_error_sweet',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
    this.subscription.globleEvent(error_ob);
 }
 enableForm(){
  let that = this;
  that.formload = false;
  setTimeout(function(){
    that.formload = false;
  },1000);
}

}
