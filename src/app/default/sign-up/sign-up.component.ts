import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as moment from 'moment';

import { AuthApiService } from '../../auth-api.service';
import { CommonEventsService } from "../../common-events.service";
import { ccodes } from "./../../c-codes";
import { AddChildValidation } from '../../service/validation/add-child-validation';



@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
  ],
})
export class SignUpComponent implements OnInit {

  elements: Elements;
  card: StripeElement;
  elementsOptions: ElementsOptions = {
    locale: 'en'
  };
  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  signinForm : FormGroup;
  submitted = false;
  submitted1 = false;
  submitted2 = false;
  submitted3 = false;
  submitted4 = false;
  formload = false;

  form_step :number =1;
  completed_step :number =1;
  previewImg : any= "";
  card_validation : any= "";

  phoOb :any = null;
  country_code : any = "+1";

  is_exist_parent : boolean = false;
  is_parent_call : boolean = false;

  is_exist_email : boolean = false;
  is_exist_phone : boolean = false;

  signupForm : FormGroup;
  files: File[] = [];

  maxDate: Date = new Date(new Date().setDate(new Date().getDate() - 365*18));
  minDate: Date = new Date(new Date().setDate(new Date().getDate() - 365*18));

  constructor(formBuilder: FormBuilder,public api: AuthApiService,private subscription: CommonEventsService,private stripeService: StripeService) {
    this.signupForm = formBuilder.group({
      'country_code':null,
      'token_id': [null, Validators.required],
      'term': [null, Validators.required],
      'card_holder_name':  [null, Validators.required],
      'image': [null, Validators.required],
      'documents': [null, Validators.required],
      'email' : [null, Validators.compose([Validators.required, Validators.email])],
      'first_name' : [null, Validators.required],
      'last_name': [null, Validators.required],
      'dob': [null, Validators.required],
      'gender' : ['male', Validators.required],
      'phone' : [null, Validators.compose([Validators.required,Validators.maxLength(15)])],
      'state' : [null, Validators.required],
      'city' : [null, Validators.required],
      'password' : [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      'confirm_password' : []
    },{'validator': AddChildValidation.MatchPassword});

  }

  private prepareSave(formData: any): any {

    let input = new FormData();
    input.append('card_holder_name',this.signupForm.get('card_holder_name').value);
    input.append('token_id',this.signupForm.get('token_id').value);
    input.append('first_name',this.signupForm.get('first_name').value);
    input.append('last_name', this.signupForm.get('last_name').value);
    input.append('password', this.signupForm.get('password').value);
    input.append('confirm_password', this.signupForm.get('confirm_password').value);
    input.append('email', this.signupForm.get('email').value);
    input.append('gender', this.signupForm.get('gender').value);
    input.append('city', this.signupForm.get('city').value);
    input.append('state', this.signupForm.get('state').value);

    input.append('country_code', this.country_code);
    input.append('devicetype', "web");
    input.append('dob',moment(this.signupForm.get('dob').value).format('YYYY-MM-DD'));


    if (this.signupForm.get('phone').value) {
      let aphone = this.signupForm.get('phone').value;
      input.append('phone', aphone.replace(/[^0-9]/g, ''));
    }
    if (this.signupForm.get('image').value) {
      input.append('image', this.signupForm.get('image').value);
    }
    if (this.files.length) {
      for (let i = 0; i < this.files.length; i++) {
        input.append('documents[]', this.files[i]);
      }
    }


    return input;
  }

  ngOnInit() {}
  ngAfterViewInit() {
    this.stripeService.elements(this.elementsOptions)
    .subscribe(elements => {
      this.elements = elements;
      // Only mount the element the first time
      if (!this.card) {
        this.card = this.elements.create('card', {
          style: {
            base: {
              iconColor: '#666EE8',
              color: '#31325F',
              lineHeight: '40px',
              fontWeight: 300,
              fontSmoothing: 'antialiased',
              fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
              fontSize: '18px',
              '::placeholder': {
                color: 'purple'
              }
            }
          }
        });
        this.card.mount('#card-element');
      }
    });
  }
  gotoRoute(route:string) {
    this.subscription.globleEvent({action:'redirect',redirect_to:route,dataobj:{}});
  }
  gotostp(nextstep:number) {
    if(this.form_step ==1 && !this.isForm1Valid()){
      return false;
    }
    if(this.form_step ==2 && !this.isForm2Valid()){
      return false;
    }
    if(this.form_step ==3 && !this.isForm3Valid()){
      return false;
    }

    if(this.form_step ==4 && nextstep ==5){
      this.processStep4();
    }else{
       this.form_step = nextstep;
    }
  }
  getNumber(obj) {
    // console.log(obj);
   }
   dochangeCountry(dcode:any){
   for (let i = 0; i < ccodes.length; i++) {
        if (ccodes[i].dial_code == dcode) {
       this.phoOb.intlTelInput('setCountry',ccodes[i].code);
       break;
        }
     }

   }
   onCountryChange(obj) {
    this.country_code = "+"+obj.dialCode;
   }

   hasError(obj) {
     //console.log(obj);
   }
   telInputObject(obj) {
     this.phoOb = obj;
     this.dochangeCountry(this.country_code);
   }

   onFileChange(event:any) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.signupForm.get('image').setValue(file);

      var reader = new FileReader();

      reader.onload = (event) => {
        this.previewImg = (<FileReader>event.target).result;
      }
      reader.readAsDataURL(event.target.files[0]);
    } else {
      this.signupForm.get('image').setValue(null);
      this.previewImg = "";
    }


  }

  addEvent(type: string, event) {
    //this.signupForm.get('dob').setValue(moment(event.value).format('YYYY-MM-DD'));
  }
  onSelect(event) {
    this.files.push(...event.addedFiles);

    if(this.files.length <=0){
      this.signupForm.get('documents').setValue(null);
    }else{
      this.signupForm.get('documents').setValue(this.files);
    }

  }

  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
    if(this.files.length <=0){
      this.signupForm.get('documents').setValue(null);
    }else{
      this.signupForm.get('documents').setValue(this.files);
    }
  }

   isForm1Valid(){
    let res = true;
    this.submitted1 = true;

    if(this.signupForm.get('phone').invalid || this.signupForm.get('email').invalid || this.signupForm.get('first_name').invalid || this.signupForm.get('last_name').invalid){
      res = false;
    }else if(this.signupForm.get('password').invalid || this.signupForm.get('confirm_password').invalid){
      res = false;
    }else if(this.is_exist_email || this.is_exist_phone){
      res = false;
    }
    return res;
   }
   isForm2Valid(){
    let res = true;
    this.submitted2 = true;

    if(this.signupForm.get('image').invalid || this.signupForm.get('gender').invalid || this.signupForm.get('dob').invalid || this.signupForm.get('state').invalid || this.signupForm.get('city').invalid){
      res = false;
    }
    return res;
   }
   isForm3Valid(){
    let res = true;
    this.submitted3 = true;

    if(this.signupForm.get('documents').invalid){
      res = false;
    }
    return res;
   }
   processStep4() {
    this.submitted4 = true;
    const name = this.signupForm.value.card_holder_name;
    const term = this.signupForm.value.term;
    if(!name || name == "" || !term || term == ""){
      return false;
    }
    this.formload =true;
    this.card_validation = "";

    this.stripeService
      .createToken(this.card, { name })
      .subscribe(result => {
        if (result.token) {
            this.signupForm.get('token_id').setValue(result.token.id);
            this.onSubmit();
        } else if (result.error) {
          this.formload = false;
          this.card_validation = result.error.message;
        }
      });
  }

  onSubmit() {
    let formData =this.signupForm.value;
    this.submitted = true;

    if (this.signupForm.invalid) {
      return;
    }else{

      let formvalue = this.prepareSave(formData);

      this.api.formObjectSubmit("register",formvalue).subscribe(result => {
        this.formload = false;
        let sucsess_ob =  {action:'flash_success_sweet',redirect_to:'sign-in',dataobj:{'message':result.message,'message_head':'Success !'}};
        this.subscription.globleEvent(sucsess_ob);

      },
      (error) => { this.formload = false; this.displayError(error);})
   }

 }

 displayError(error:any) {
  let errMsg = (error.message) ? error.message :
  error.status ? `${error.status} - ${error.statusText}` : 'Server error';

  if(error.error && error.error.message){
    errMsg = error.error.message
  }
  let error_ob =  {action:'flash_error_sweet',redirect_to:'',dataobj:{'message':errMsg,'message_head':'Request Not Procceed !'}};
  this.subscription.globleEvent(error_ob);
}

   verifyForm(field:string) {

    let modal = "unique-field/"+field;
    let formdata = {};

    let fv = "";
    if(field == 'email'){
      this.is_exist_email = false;
      fv = this.signupForm.value.email;
      formdata = {field_value:fv,on_exist:'false'};
    }else if(field == 'phone'){
      this.is_exist_phone = false;
       fv = this.signupForm.value.phone.replace(/[^0-9]/g, '');
       formdata = {field_value:fv,on_exist:'false'};
    }

    if(fv==""){
      return true;
    }
    this.is_parent_call = true;

    this.api.submitAuth(modal,formdata).subscribe(result => {
      this.is_parent_call = false;
      if(field == 'email'){
        this.is_exist_email = false;
      }if(field == 'phone'){
        this.is_exist_phone = false;
      }
    },
    (error) => {
      this.is_parent_call = false;
      if(field == 'email'){
        this.is_exist_email = true;
      }if(field == 'phone'){
        this.is_exist_phone = true;
      }

     })
  }

}
