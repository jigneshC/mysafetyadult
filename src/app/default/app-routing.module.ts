import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './layout/layout.component';

import { LandingComponent } from './landing/landing.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { FeaturesComponent } from './features/features.component';
import { PricingComponent } from './pricing/pricing.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { SupportComponent } from './support/support.component';
import { AnonymousComplaintComponent } from './anonymous-complaint/anonymous-complaint.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';

const routes: Routes = [

  {path: '', component:LayoutComponent, children: [
    {
      path: '',
      component: LandingComponent
    },
    {
      path: 'sign-in',
      component: SignInComponent
    },
    {
      path: 'sign-up',
      component: SignUpComponent
    },
    {
      path: 'forgot-password',
      component: ForgotPasswordComponent
    },
    {
      path: 'contact',
      component: ContactComponent
    },
	{
	  path: 'blogs',
	  component: BlogsComponent
	},
	{
	  path: 'blog/:id',
	  component: BlogDetailComponent
	},
	{
	  path: 'about',
	  component: AboutComponent
	},
	{
	  path: 'contact',
	  component: ContactComponent
	},
	{
	  path: 'anonymous-complaint',
	  component: AnonymousComplaintComponent
	},
    {
	  path: 'privacy',
	  component: PrivacyComponent
	},
	{
	  path: 'terms-conditions',
	  component: TermsConditionsComponent
	},
	{
	  path: 'support',
	  component: SupportComponent
	},
	{
	  path: 'features',
	  component: FeaturesComponent
	},
	{
	  path: 'pricing',
	  component: PricingComponent
	}
  ]}

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

