import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router} from '@angular/router';
import { NgwWowService } from 'ngx-wow';
import { CommonEventsService } from "../../common-events.service";
import { AuthApiService } from '../../auth-api.service';


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(private wowService: NgwWowService,public api: AuthApiService, private route: ActivatedRoute, private router: Router, private subscription: CommonEventsService) {
		this.wowService.init();
  }
  
  

  ngOnInit() {
	this.loadScript('assets/js/owl.carousel.min.js');
	this.loadScript('assets/js/get-owlslider.js');
  }
  
  public loadScript(url) {
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
  }

  gotoRoute(route: string, isselfpage: boolean) {

	let sucsess_ob =  {action:'',redirect_to:route,dataobj:{'message':'','message_head':''}};
    this.subscription.globleEvent(sucsess_ob);
	
  }
  
}
