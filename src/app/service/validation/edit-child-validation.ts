import {AbstractControl} from '@angular/forms';
export class EditChildValidation {

    static Formvalidation(AC: AbstractControl) {
        let parent_mob_no = AC.get('phone').value; // to get value in input tag
        if (parent_mob_no) {
            var number = parent_mob_no.replace(/[^0-9]/g, '');

            if (number.length != 10) {
                AC.get('phone').setErrors({ PhoneTen: true })
            }
        }
    }
}