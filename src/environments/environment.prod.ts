export const environment = {
  production: true,
  firebase: {
    apiKey: "8e3638820945bcc7b9fdac98f66a7142a462ad13",
		authDomain: "mysafetynet-f1902.firebaseapp.com",
		databaseURL: "https://mysafetynet-f1902.firebaseio.com/",
		projectId: "mysafetynet-f1902",
		storageBucket: "lfk-parent.appspot.com",
		messagingSenderId: "859740767333"
  },
  gaConfig: {
    trackingId: 'AW-763140610',
    debug: true
  }
};
